#[cfg(test)]
mod tests;

use crate::AnsiSequence;

use core::convert::TryInto;
use heapless::Vec;
use nom::branch::alt;
use nom::bytes::streaming::tag;
use nom::character::streaming::{digit0, digit1};
use nom::combinator::{map_res, opt};
use nom::error::ErrorKind;
use nom::*;

// TODO: copied from rust-nom@4.2.3 -- what should it be changed to
// when using combinators instead of macros?
macro_rules! expr_res (
  ($i:expr, $e:expr) => (
    {
      match $e {
        Ok(output) => Ok(($i, output)),
	// TODO: what should be the error for a failed Vec::from_slice?
	// ErrorKind::ExprRes has been removed in rust-nom@7.0.0
        Err(_)     => Err(Err::Error(error_position!($i, ErrorKind::TooLarge)))
      }
    }
  )
);

macro_rules! tag_parser {
    ($sig:ident, $tag:expr, $ret:expr) => {
        fn $sig(input: &str) -> IResult<&str, AnsiSequence> {
            let (input, _) = tag($tag)(input)?;
            Ok((input, $ret))
        }
    };
}

fn parse_int(input: &str) -> IResult<&str, u32> {
    combinator::map_res(digit1, |s: &str| s.parse::<u32>())(input)
}

// TODO kind of ugly, would prefer to pass in the default so we could use it for
// all escapes with defaults (not just those that default to 1).
fn parse_def_cursor_int(input: &str) -> IResult<&str, u32> {
    combinator::map(digit0, |s: &str| s.parse::<u32>().unwrap_or(1))(input)
}

fn cursor_pos(input: &str) -> IResult<&str, AnsiSequence> {
    let (input, _) = tag("[")(input)?;
    let (input, x) = parse_def_cursor_int(input)?;
    let (input, _) = opt(tag(";"))(input)?;
    let (input, y) = parse_def_cursor_int(input)?;
    let (input, _) = alt((tag("H"), tag("f")))(input)?;
    Ok((input, AnsiSequence::CursorPos(x, y)))
}

fn escape(input: &str) -> IResult<&str, AnsiSequence> {
    let (input, _) = tag("\u{1b}")(input)?;
    Ok((input, AnsiSequence::Escape))
}

fn cursor_up(input: &str) -> IResult<&str, AnsiSequence> {
    let (input, _) = tag("[")(input)?;
    let (input, am) = parse_def_cursor_int(input)?;
    Ok((input, AnsiSequence::CursorUp(am)))
}

fn cursor_down(input: &str) -> IResult<&str, AnsiSequence> {
    let (input, _) = tag("[")(input)?;
    let (input, am) = parse_def_cursor_int(input)?;
    let (input, _) = tag("B")(input)?;
    Ok((input, AnsiSequence::CursorDown(am)))
}

fn cursor_forward(input: &str) -> IResult<&str, AnsiSequence> {
    let (input, _) = tag("[")(input)?;
    let (input, am) = parse_def_cursor_int(input)?;
    let (input, _) = tag("C")(input)?;
    Ok((input, AnsiSequence::CursorForward(am)))
}

fn cursor_backward(input: &str) -> IResult<&str, AnsiSequence> {
    let (input, _) = tag("[")(input)?;
    let (input, am) = parse_def_cursor_int(input)?;
    let (input, _) = tag("D")(input)?;
    Ok((input, AnsiSequence::CursorBackward(am)))
}

fn graphics_mode1(input: &str) -> IResult<&str, AnsiSequence> {
    let (input, _) = tag("[")(input)?;
    let (input, val) = parse_int(input)?;
    let (input, _) = tag("m")(input)?;
    let (input, val) = expr_res!(input, val.try_into())?;
    let (input, conv) = expr_res!(input, Vec::from_slice(&[val]))?;
    Ok((input, AnsiSequence::SetGraphicsMode(conv)))
}

fn graphics_mode2(input: &str) -> IResult<&str, AnsiSequence> {
    let (input, _) = tag("[")(input)?;
    let (input, val1) = parse_int(input)?;
    let (input, _) = tag(";")(input)?;
    let (input, val2) = parse_int(input)?;
    let (input, _) = tag("m")(input)?;
    let (input, val1) = expr_res!(input, val1.try_into())?;
    let (input, val2) = expr_res!(input, val2.try_into())?;
    let (input, conv) = expr_res!(input, Vec::from_slice(&[val1, val2,]))?;
    Ok((input, AnsiSequence::SetGraphicsMode(conv)))
}

fn graphics_mode3(input: &str) -> IResult<&str, AnsiSequence> {
    let (input, _) = tag("[")(input)?;
    let (input, val1) = parse_int(input)?;
    let (input, _) = tag(";")(input)?;
    let (input, val2) = parse_int(input)?;
    let (input, _) = tag(";")(input)?;
    let (input, val3) = parse_int(input)?;
    let (input, _) = tag("m")(input)?;
    let (input, val1) = expr_res!(input, val1.try_into())?;
    let (input, val2) = expr_res!(input, val2.try_into())?;
    let (input, val3) = expr_res!(input, val3.try_into())?;
    let (input, conv) = expr_res!(input, Vec::from_slice(&[val1, val2, val3,]))?;
    Ok((input, AnsiSequence::SetGraphicsMode(conv)))
}

fn graphics_mode4(input: &str) -> IResult<&str, AnsiSequence> {
    let (input, _) = tag("[m")(input)?;
    Ok((input, AnsiSequence::SetGraphicsMode(Vec::new())))
}

fn graphics_mode5(input: &str) -> IResult<&str, AnsiSequence> {
    let (input, _) = tag("[")(input)?;
    let (input, val1) = map_res(parse_int, |s| s.try_into())(input)?;
    let (input, _) = tag(";")(input)?;
    let (input, val2) = map_res(parse_int, |s| s.try_into())(input)?;
    let (input, _) = tag(";")(input)?;
    let (input, val3) = map_res(parse_int, |s| s.try_into())(input)?;
    let (input, _) = tag(";")(input)?;
    let (input, val4) = map_res(parse_int, |s| s.try_into())(input)?;
    let (input, _) = tag(";")(input)?;
    let (input, val5) = map_res(parse_int, |s| s.try_into())(input)?;
    let (input, _) = tag("m")(input)?;
    let (input, conv) = expr_res!(input, Vec::from_slice(&[val1, val2, val3, val4, val5,]))?;
    Ok((input, AnsiSequence::SetGraphicsMode(conv)))
}

fn graphics_mode(input: &str) -> IResult<&str, AnsiSequence> {
    alt((
        graphics_mode1,
        graphics_mode2,
        graphics_mode3,
        graphics_mode4,
        graphics_mode5,
    ))(input)
}

fn set_mode(input: &str) -> IResult<&str, AnsiSequence> {
    let (input, _) = tag("[=")(input)?;
    let (input, conv) = map_res(parse_int, |s| s.try_into())(input)?;
    let (input, _) = tag("h")(input)?;
    Ok((input, AnsiSequence::SetMode(conv)))
}

fn reset_mode(input: &str) -> IResult<&str, AnsiSequence> {
    let (input, _) = tag("[=")(input)?;
    let (input, conv) = map_res(parse_int, |s| s.try_into())(input)?;
    let (input, _) = tag("l")(input)?;
    Ok((input, AnsiSequence::ResetMode(conv)))
}

fn set_top_and_bottom(input: &str) -> IResult<&str, AnsiSequence> {
    let (input, _) = tag("[")(input)?;
    let (input, x) = parse_int(input)?;
    let (input, _) = tag(";")(input)?;
    let (input, y) = parse_int(input)?;
    let (input, _) = tag("r")(input)?;
    Ok((input, AnsiSequence::SetTopAndBottom(x, y)))
}

tag_parser!(cursor_save, "[s", AnsiSequence::CursorSave);
tag_parser!(cursor_restore, "[u", AnsiSequence::CursorRestore);
tag_parser!(erase_display, "[2J", AnsiSequence::EraseDisplay);
tag_parser!(erase_line, "[K", AnsiSequence::EraseLine);
tag_parser!(hide_cursor, "[?25l", AnsiSequence::HideCursor);
tag_parser!(show_cursor, "[?25h", AnsiSequence::ShowCursor);
tag_parser!(cursor_to_app, "[?1h", AnsiSequence::CursorToApp);
tag_parser!(set_new_line_mode, "[20h", AnsiSequence::SetNewLineMode);
tag_parser!(set_col_132, "[?3h", AnsiSequence::SetCol132);
tag_parser!(set_smooth_scroll, "[?4h", AnsiSequence::SetSmoothScroll);
tag_parser!(set_reverse_video, "[?5h", AnsiSequence::SetReverseVideo);
tag_parser!(set_origin_rel, "[?6h", AnsiSequence::SetOriginRelative);
tag_parser!(set_auto_wrap, "[?7h", AnsiSequence::SetAutoWrap);
tag_parser!(set_auto_repeat, "[?8h", AnsiSequence::SetAutoRepeat);
tag_parser!(set_interlacing, "[?9h", AnsiSequence::SetInterlacing);
tag_parser!(set_linefeed, "[20l", AnsiSequence::SetLineFeedMode);
tag_parser!(set_cursorkey, "[?1l", AnsiSequence::SetCursorKeyToCursor);
tag_parser!(set_vt52, "[?2l", AnsiSequence::SetVT52);
tag_parser!(set_col80, "[?3l", AnsiSequence::SetCol80);
tag_parser!(set_jump_scroll, "[?4l", AnsiSequence::SetJumpScrolling);
tag_parser!(set_normal_video, "[?5l", AnsiSequence::SetNormalVideo);
tag_parser!(set_origin_abs, "[?6l", AnsiSequence::SetOriginAbsolute);
tag_parser!(reset_auto_wrap, "[?7l", AnsiSequence::ResetAutoWrap);
tag_parser!(reset_auto_repeat, "[?8l", AnsiSequence::ResetAutoRepeat);
tag_parser!(reset_interlacing, "[?9l", AnsiSequence::ResetInterlacing);

tag_parser!(set_alternate_keypad, "=", AnsiSequence::SetAlternateKeypad);
tag_parser!(set_numeric_keypad, ">", AnsiSequence::SetNumericKeypad);
tag_parser!(set_uk_g0, "(A", AnsiSequence::SetUKG0);
tag_parser!(set_uk_g1, ")A", AnsiSequence::SetUKG1);
tag_parser!(set_us_g0, "(B", AnsiSequence::SetUSG0);
tag_parser!(set_us_g1, ")B", AnsiSequence::SetUSG1);
tag_parser!(set_g0_special, "(0", AnsiSequence::SetG0SpecialChars);
tag_parser!(set_g1_special, ")0", AnsiSequence::SetG1SpecialChars);
tag_parser!(set_g0_alternate, "(1", AnsiSequence::SetG0AlternateChar);
tag_parser!(set_g1_alternate, ")1", AnsiSequence::SetG1AlternateChar);
tag_parser!(set_g0_graph, "(2", AnsiSequence::SetG0AltAndSpecialGraph);
tag_parser!(set_g1_graph, ")2", AnsiSequence::SetG1AltAndSpecialGraph);
tag_parser!(set_single_shift2, "N", AnsiSequence::SetSingleShift2);
tag_parser!(set_single_shift3, "O", AnsiSequence::SetSingleShift3);

fn combined(input: &str) -> IResult<&str, AnsiSequence> {
    // alt supports only 21 cases, so avoid the limit by nesting them
    alt((
        alt((
            escape,
            cursor_pos,
            cursor_up,
            cursor_down,
            cursor_forward,
            cursor_backward,
            cursor_save,
            cursor_restore,
            erase_display,
            erase_line,
            graphics_mode,
            set_mode,
            reset_mode,
            hide_cursor,
            show_cursor,
            cursor_to_app,
        )),
        alt((
            set_new_line_mode,
            set_col_132,
            set_smooth_scroll,
            set_reverse_video,
            set_origin_rel,
            set_auto_wrap,
            set_auto_repeat,
            set_interlacing,
            set_linefeed,
            set_cursorkey,
            set_vt52,
            set_col80,
            set_jump_scroll,
            set_normal_video,
            set_origin_abs,
        )),
        alt((
            reset_auto_wrap,
            reset_auto_repeat,
            reset_interlacing,
            set_top_and_bottom,
            set_alternate_keypad,
            set_numeric_keypad,
            set_uk_g0,
            set_uk_g1,
            set_us_g0,
            set_us_g1,
            set_g0_special,
            set_g1_special,
            set_g0_alternate,
            set_g1_alternate,
            set_g0_graph,
            set_g1_graph,
            set_single_shift2,
            set_single_shift3,
        )),
    ))(input)
}

pub fn parse_escape(input: &str) -> IResult<&str, AnsiSequence> {
    let (input, _) = tag("\u{1b}")(input)?;
    let (input, seq) = combined(input)?;
    Ok((input, seq))
}
